package com.redfox.gesturedetector

import android.bluetooth.BluetoothDevice
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.redfox.gesturedetector.util.CommandSender
import kotlinx.android.synthetic.main.activity_bluetooth_connect.*
import kotlinx.android.synthetic.main.activity_main.*
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.Socket
import java.util.*

class BluetoothConnectActivity : AppCompatActivity() {
    val MY_UUID: UUID = UUID.fromString("0000110E-0000-1000-8000-00805F9B34FB")

    var bluetoothDevice: BluetoothDevice? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bluetooth_connect)

//        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        for (device in bluetoothAdapter.bondedDevices) {
//            if (device.address == "B8:27:EB:E0:55:9D") {
//                bluetoothDevice = device
//            }
//        }
//        if (bluetoothDevice != null) {
//            val socket = bluetoothDevice!!.createRfcommSocketToServiceRecord(MY_UUID)
//            socket.connect()
//
//            val inputStream = socket.inputStream
//            val outputStream = socket.outputStream
//        }

        openSocket.setOnClickListener {
            Thread({
                var socket = Socket()
                try {
                    val host = addressInput.text.toString()
                    val port = portInput.text.toString().toInt()
                    socket.connect(InetSocketAddress(InetAddress.getByName(host), port), 5000)
                } catch (e: Exception) {
                    runOnUiThread({
                        Toast.makeText(this, "Cannot connect to specified server", Toast.LENGTH_SHORT).show()
                    })
                }
                if (socket.isConnected) {
                    CommandSender.getInstance().socket = socket
                    finish()
                }
            }).start()
        }
    }
}
