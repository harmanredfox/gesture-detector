package com.redfox.gesturedetector

import android.content.Intent
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import com.konstantinmaryin.redfoxgesturedetector.observing.SensorEventObservableFactory
import com.redfox.gesturedetector.util.*
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {
    val THRESHOLD = 20f
    val SHAKES_COUNT = 5
    val SHAKES_PERIOD = 1
    val TIME_THRESHOLD = 1e8
    val ORIENTATION_THRESHOLD = Math.PI / 4
    val LOW_PASS_FILTER_ALPHA = 0.05f

    lateinit var speechEngine: TextToSpeech

    private var gravity: FloatArray? = null
    private var geomagnetic: FloatArray? = null
    private var rotationMatrix = FloatArray(9)
    private var inclinationMatrix = FloatArray(9)
    private var orientationValues: FloatArray? = null

    private var currentOrientation = MotionOrientation.UNKNOWN

    var dump: ArrayList<FloatArray>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        speechEngine = TextToSpeech(this, { status ->
            if (status != TextToSpeech.ERROR) {
                speechEngine.language = Locale.US
            }
        })

        graphView.ordinateExtremum = 50

        SensorEventObservableFactory.createLinearAccelerationObservable(this)
                .observeOn(AndroidSchedulers.mainThread())
                .map { event -> XEvent(event.timestamp, event.values[0]) }
                .filter{ xEvent -> Math.abs(xEvent.xValue) > THRESHOLD }
                .buffer(2, 1)
                .filter{ buffer -> buffer[0].xValue * buffer[1].xValue < 0 }
                .map{ buffer -> buffer[1].timestamp / 1000000000f }
                .buffer(SHAKES_COUNT, 1)
                .filter { buffer -> buffer[SHAKES_COUNT - 1] - buffer[0] < SHAKES_PERIOD }
                .subscribe( {
                    if (currentOrientation == MotionOrientation.LOUDSPEAKER) {
                        speechEngine.speak(MotionEvent.PAUSE.speechText, TextToSpeech.QUEUE_FLUSH, null)
                    }
                })
        SensorEventObservableFactory.createLinearAccelerationObservable(this)
                .observeOn(AndroidSchedulers.mainThread())
                .map { event -> TravelEvent(event.timestamp, event.values[2]) }
                .filter { event -> Math.abs(event.acceleration) >= THRESHOLD }
                .buffer(2, 1)
                .filter { eventBuffer -> eventBuffer[0].acceleration * eventBuffer[1].acceleration < 0
                        && eventBuffer[1].timestamp - eventBuffer[0].timestamp < TIME_THRESHOLD }
                .subscribe({ eventBuffer ->
                    var motionEvent: MotionEvent? = null
                    when(currentOrientation) {
                        MotionOrientation.SLIDING -> {
                            motionEvent = if (eventBuffer[0].acceleration < eventBuffer[1].acceleration)
                                MotionEvent.SLIDE_RIGHT else MotionEvent.SLIDE_LEFT
                            speechEngine.speak(motionEvent.speechText, TextToSpeech.QUEUE_FLUSH, null)

                            Thread({
                                val socket = CommandSender.getInstance().socket
                                if (socket != null) {
                                    val text = motionEvent!!.speechText.toByteArray()
                                    socket.getOutputStream().write(text)
                                }
                            }).start()
                        }
                        MotionOrientation.LOUDSPEAKER -> {
                            motionEvent = if (eventBuffer[0].acceleration < eventBuffer[1].acceleration)
                                MotionEvent.SLIDE_DOWN else MotionEvent.SLIDE_UP
                            speechEngine.speak(motionEvent.speechText, TextToSpeech.QUEUE_FLUSH, null)
                        }
                    }
                })

        val magneticFieldObservable = SensorEventObservableFactory.createMagneticFieldObservable(this)
                .observeOn(AndroidSchedulers.mainThread())
        magneticFieldObservable.subscribe({ event ->
                    geomagnetic = event.values
                })
        val accelerometerObservable = SensorEventObservableFactory.createAccelerometerObservable(this)
                .observeOn(AndroidSchedulers.mainThread())
        accelerometerObservable.subscribe({ event ->
                    gravity = event.values
                })
        magneticFieldObservable.mergeWith(accelerometerObservable)
                .filter{ gravity != null && geomagnetic != null }
                .map { _ ->
                    if (SensorManager.getRotationMatrix(rotationMatrix, inclinationMatrix, gravity, geomagnetic)) {
                        val orientation: FloatArray = kotlin.FloatArray(3)
                        return@map SensorManager.getOrientation(rotationMatrix, orientation)
                    }
                    return@map FloatArray(3)
                }
                .subscribe({ orientation ->
                    if (orientationValues != null) {
                        orientationValues!![0] = orientationValues!![0] + LOW_PASS_FILTER_ALPHA * (orientation[0] - orientationValues!![0])
                        orientationValues!![1] = orientationValues!![1] + LOW_PASS_FILTER_ALPHA * (orientation[1] - orientationValues!![1])
                        orientationValues!![2] = orientationValues!![2] + LOW_PASS_FILTER_ALPHA * (orientation[2] - orientationValues!![2])
                        if (compareAngles(MotionOrientation.SLIDING.pitchValue, orientationValues!![1]) &&
                                compareAngles(MotionOrientation.SLIDING.rollValue, orientationValues!![2])) {
                            currentOrientation = MotionOrientation.SLIDING
                        } else if (compareAngles(MotionOrientation.LOUDSPEAKER.pitchValue, orientationValues!![1]) &&
                                compareAngles(MotionOrientation.LOUDSPEAKER.rollValue, orientationValues!![2])) {
                            currentOrientation = MotionOrientation.LOUDSPEAKER
                        } else {
                            currentOrientation = MotionOrientation.UNKNOWN
                        }
                    } else {
                        orientationValues = orientation
                    }
                })

        clearButton.setOnClickListener { graphView.clear() }

        connectButton.setOnClickListener {
            startActivity(Intent(this, BluetoothConnectActivity::class.java))
        }
    }

    fun compareAngles(alpha: Float, beta: Float): Boolean {
        val phi = Math.abs(beta - alpha) % Math.PI
        val distance = if (phi > Math.PI) Math.PI * 2 - phi else phi

        return distance < ORIENTATION_THRESHOLD
    }
}
