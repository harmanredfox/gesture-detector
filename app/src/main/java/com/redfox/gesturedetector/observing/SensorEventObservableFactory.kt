package com.konstantinmaryin.redfoxgesturedetector.observing

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import io.reactivex.Observable
import io.reactivex.ObservableEmitter

/**
 * Created by konstantinmaryin on 02/12/2017.
 */
open class SensorEventObservableFactory {
    companion object {
        private fun create(sensor: Sensor, sensorManager: SensorManager, delay: Int): Observable<SensorEvent> {
            return Observable.create({ subscriber ->
                val listener = ObservableSensorEventListener(subscriber)
                sensorManager.registerListener(listener, sensor, delay)

                subscriber.setCancellable {
                    sensorManager.unregisterListener(listener)
                }
            })
        }

        open fun createLinearAccelerationObservable(context: Context): Observable<SensorEvent> {
            val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
            val sensorList = sensorManager.getSensorList(Sensor.TYPE_LINEAR_ACCELERATION)
            if (sensorList == null || sensorList.isEmpty()) {
                throw IllegalStateException("Device has no linear acceleration sensor")
            }

            return SensorEventObservableFactory.create(sensorList[0], sensorManager, SensorManager.SENSOR_DELAY_GAME)
        }

        open fun createMagneticFieldObservable(context: Context): Observable<SensorEvent> {
            val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
            val sensorList = sensorManager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD)
            if (sensorList == null || sensorList.isEmpty()) {
                throw IllegalStateException("Device has no magnetic field sensor")
            }

            return SensorEventObservableFactory.create(sensorList[0], sensorManager, SensorManager.SENSOR_DELAY_NORMAL)
        }

        open fun createAccelerometerObservable(context: Context): Observable<SensorEvent> {
            val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
            val sensorList = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER)
            if (sensorList == null || sensorList.isEmpty()) {
                throw IllegalStateException("Device has no accelerometer sensor")
            }

            return SensorEventObservableFactory.create(sensorList[0], sensorManager, SensorManager.SENSOR_DELAY_NORMAL)
        }
    }

    private class ObservableSensorEventListener constructor(private val subscriber: ObservableEmitter<SensorEvent>): SensorEventListener {

        override fun onSensorChanged(event: SensorEvent) {
            if (subscriber.isDisposed) {
                return
            }

            subscriber.onNext(event)
        }

        override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
            //Do nothing
        }
    }
}