package com.redfox.gesturedetector.piui;

import java.net.InetAddress;

/**
 * Created by konstantinmaryin on 06/12/2017.
 */

public interface PiPollerAddressListener {
    void onPiFoundAtAddress(InetAddress addr);
    void onStatusUpdate(String status);
    void onConnectionLost();
    void onAppFound();
}
