package com.redfox.gesturedetector.util;

import java.net.Socket;

/**
 * Created by KMaryin on 12/6/2017.
 */

public class CommandSender {
    private static volatile CommandSender instance;
    private Socket socket;

    public static CommandSender getInstance() {
        CommandSender localInstance = instance;
        if (localInstance == null) {
            synchronized (CommandSender.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new CommandSender();
                }
            }
        }
        return localInstance;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public Socket getSocket() {
        return socket;
    }
}
