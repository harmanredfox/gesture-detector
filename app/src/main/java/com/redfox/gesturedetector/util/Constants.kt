package com.redfox.gesturedetector.util

/**
 * Created by KMaryin on 12/5/2017.
 */
enum class MotionEvent(val speechText: String) {
    SLIDE_LEFT("slide left"),
    SLIDE_RIGHT("slide right"),
    SLIDE_UP("slide up"),
    SLIDE_DOWN("slide down"),
    PAUSE("pause")
}

enum class MotionOrientation(val pitchValue: Float, val rollValue: Float) {
    SLIDING(0f, -(Math.PI / 2).toFloat()),
    LOUDSPEAKER(0f, 0f),
    UNKNOWN(0f, 0f)
}