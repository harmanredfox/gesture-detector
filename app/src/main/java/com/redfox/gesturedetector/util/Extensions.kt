package com.konstantinmaryin.redfoxgesturedetector.util

import android.graphics.Canvas

/**
 * Created by konstantinmaryin on 02/12/2017.
 */

fun Canvas.minSide(): Int = if (this.width < this.height) this.width else this.height