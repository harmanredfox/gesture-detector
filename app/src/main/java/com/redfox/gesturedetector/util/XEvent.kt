package com.redfox.gesturedetector.util

/**
 * Created by KMaryin on 12/4/2017.
 */
class XEvent(val timestamp: Long, val xValue: Float)

class TravelEvent(val timestamp: Long, val acceleration: Float) {
    override fun toString(): String {
        return String.format("[Time: %d, Acceleration: %s]", timestamp, acceleration.toString())
    }
}