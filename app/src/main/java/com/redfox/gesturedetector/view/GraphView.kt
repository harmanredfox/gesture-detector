package com.konstantinmaryin.redfoxgesturedetector.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View

/**
 * Created by konstantinmaryin on 02/12/2017.
 */
class GraphView: View {
    private val ORDINATE_PADDING = 0.05f
    private val ABSCISS_POINTS_COUNT = 100
    private val AXIS_DASH_SIZE = 30f
    private val DASH_COUNT = 5
    private val TEXT_SIZE = 25f

    private val axisPaint: Paint = Paint()
    private val redGraphPaint = Paint()
    private val greenGraphPaint = Paint()
    private val blueGraphPaint = Paint()
    private val redDrawablePoints = ArrayList<Float>()
    private val greenDrawablePoints = ArrayList<Float>()
    private val blueDrawablePoints = ArrayList<Float>()

    var ordinateExtremum: Int = 20

    constructor(context: Context): super(context)
    constructor(context: Context, attributeSet: AttributeSet): super(context, attributeSet)
    constructor(context: Context, attributeSet: AttributeSet, defStyleAttr: Int): super(context, attributeSet, defStyleAttr)

    init {
        axisPaint.color = Color.BLACK
        axisPaint.strokeWidth = 5f
        axisPaint.isAntiAlias = true
        axisPaint.style = Paint.Style.STROKE
        axisPaint.textSize = TEXT_SIZE

        redGraphPaint.color = Color.RED
        redGraphPaint.strokeWidth = 5f
        redGraphPaint.isAntiAlias = true
        redGraphPaint.style = Paint.Style.STROKE

        greenGraphPaint.color = Color.GREEN
        greenGraphPaint.strokeWidth = 5f
        greenGraphPaint.isAntiAlias = true
        greenGraphPaint.style = Paint.Style.STROKE

        blueGraphPaint.color = Color.BLUE
        blueGraphPaint.strokeWidth = 5f
        blueGraphPaint.isAntiAlias = true
        blueGraphPaint.style = Paint.Style.STROKE
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        if (canvas == null) return

        val centerX = canvas.width * ORDINATE_PADDING
        val centerY = canvas.height * 0.5f

        canvas.drawLine(centerX, 0f, centerX, canvas.height.toFloat(), axisPaint)

        val dashStep = canvas.height / (DASH_COUNT * 2 + 2).toFloat()

        var dashStartX = centerX - AXIS_DASH_SIZE / 2
        var dashEndX = centerX + AXIS_DASH_SIZE / 2
        canvas.drawLine(dashStartX, centerY, dashEndX, centerY, axisPaint)
        for (point in 1..DASH_COUNT) {
            val positiveDashY = centerY + dashStep * point
            val negativeDashY = centerY - dashStep * point

            canvas.drawLine(dashStartX, positiveDashY, dashEndX, positiveDashY, axisPaint)
            val pointString = ((ordinateExtremum / DASH_COUNT) * point).toString()
            canvas.drawText("-" + pointString, dashStartX - TEXT_SIZE / 2, positiveDashY + TEXT_SIZE / 2, axisPaint)

            canvas.drawLine(dashStartX, negativeDashY, dashEndX, negativeDashY, axisPaint)
            canvas.drawText(pointString, dashStartX - TEXT_SIZE / 2, negativeDashY + TEXT_SIZE / 2, axisPaint)
        }

        val abscissaLength = canvas.width - centerX
        val abscissaPointsDelta = (abscissaLength / ABSCISS_POINTS_COUNT) * -1
        val ordinateLength = canvas.height / 2
        var pointX = canvas.width + abscissaPointsDelta
        var path: Path? = null
        for (point in redDrawablePoints.reversed()) {
            val pointY = centerY - (ordinateLength / (ordinateExtremum + 1)) * point
            if (path == null) {
                path = Path()
                path.moveTo(pointX, pointY)
            } else {
                path.lineTo(pointX, pointY)
            }
            pointX += abscissaPointsDelta
        }
        if (path != null) {
            canvas.drawPath(path, redGraphPaint)
        }
        path = null
        pointX = canvas.width + abscissaPointsDelta
        for (point in greenDrawablePoints.reversed()) {
            val pointY = centerY - (ordinateLength / (ordinateExtremum + 1)) * point
            if (path == null) {
                path = Path()
                path.moveTo(pointX, pointY)
            } else {
                path.lineTo(pointX, pointY)
            }
            pointX += abscissaPointsDelta
        }
        if (path != null) {
            canvas.drawPath(path, greenGraphPaint)
        }
        path = null
        pointX = canvas.width + abscissaPointsDelta
        for (point in blueDrawablePoints.reversed()) {
            val pointY = centerY - (ordinateLength / (ordinateExtremum + 1)) * point
            if (path == null) {
                path = Path()
                path.moveTo(pointX, pointY)
            } else {
                path.lineTo(pointX, pointY)
            }
            pointX += abscissaPointsDelta
        }
        if (path != null) {
            canvas.drawPath(path, blueGraphPaint)
        }
    }

    fun putPoint(points: FloatArray) {
        if (points.isNotEmpty()) {
            redDrawablePoints.add(points[0])
            if (redDrawablePoints.size >= ABSCISS_POINTS_COUNT) {
                redDrawablePoints.removeAt(0)
            }
        }
        if (points.size > 1) {
            greenDrawablePoints.add(points[1])
            if (greenDrawablePoints.size >= ABSCISS_POINTS_COUNT) {
                greenDrawablePoints.removeAt(0)
            }
        }
        if (points.size > 2) {
            blueDrawablePoints.add(points[2])
            if (blueDrawablePoints.size >= ABSCISS_POINTS_COUNT) {
                blueDrawablePoints.removeAt(0)
            }
        }

        invalidate()
    }

    fun clear() {
        redDrawablePoints.clear()
        greenDrawablePoints.clear()
        blueDrawablePoints.clear()

        invalidate()
    }
}